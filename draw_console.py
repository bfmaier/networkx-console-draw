import networkx as nx
import os
from math import sqrt

class map_screen:

    def __init__(self,xmin,xmax,ymin,ymax,r,c):
        self.xmin = xmin
        self.ymin = ymin
        self.xmax = xmax
        self.ymax = ymax
        self.r = r
        self.c = c

    def map(self,x,y):
        j = int((y-self.ymin)/(self.ymax-self.ymin) * self.r)
        if j>=self.r:
            j = self.r-1
        j = self.r-1-j

        i = int((x-self.xmin)/(self.xmax-self.xmin) * self.c)
        if i>=self.c:
            i = self.c-1
        
        #return row, col
        return j,i

def draw_console(G,
                 pos=None,
                 labels=None,
                 padding=10,
                 node_radius=2):
    if pos is None:
        pos = nx.spring_layout(G)

    x = [ np[0] for np in pos.values() ]
    y = [ np[1] for np in pos.values() ]
    r,c = [ int(i) for i in os.popen('stty size', 'r').read().split(' ')]
    r -= 1+2*padding+2*int(node_radius)
    c -= 2*padding+2*int(node_radius)
    scrmap = map_screen(min(x),max(x),min(y),max(y),r,c)

    screen = [[ ' ' for j in range(c+2*int(node_radius))] for i in range(r+2*int(node_radius)) ] 

    for e in G.edges():
        write_edge_to_screen(e,pos,screen,scrmap,node_radius)

    for n in G.nodes():
        #r_,c_ = scrmap.map(*pos[n])
        #screen[r_][c_] = 'O'
        write_node_to_screen(n,pos,screen,scrmap,node_radius)

    for i in range(r+2*int(node_radius)):
        print ' '*padding+''.join(screen[i])+' '*padding


def write_node_to_screen(n,pos,screen,scrmap,node_radius=0):

    r_,c_ = scrmap.map(*pos[n])
    r_ += int(node_radius)
    c_ += int(node_radius)
    r,c = len(screen),len(screen[0])

    for m in range(max([0,r_-int(node_radius)]),min([r-1,r_+int(node_radius)])+1):
        for l in range(max([0,c_-int(node_radius)]),min([c-1,c_+int(node_radius)])+1):
            if sqrt(2*(m-r_)**2+(l-c_)**2)<=node_radius:
                screen[m][l] = u'\u2588'




def write_edge_to_screen(e,pos,screen,scrmap,node_radius):

    if node_radius==0:
        z = u'\u00B7'
    else:
        z = u'\u25AA'

    Nplot = int(sqrt(len(screen)**2+len(screen[0])**2))
    dt = 1./Nplot
    x1,y1 = pos[e[0]]
    x2,y2 = pos[e[1]]
    vecx = (x2-x1)
    vecy = (y2-y1)
    for i in range(Nplot+1):
        newx = vecx*i*dt + x1
        newy = vecy*i*dt + y1
        r,c = scrmap.map(newx,newy)
        r += int(node_radius)
        c += int(node_radius)
        screen[r][c] = z


if __name__=="__main__":

    import pylab as pl

    #G = nx.karate_club_graph()
    G = nx.cycle_graph(50)
    #G = nx.grid_2d_graph(10,10)
    G = nx.gnp_random_graph(100,.08)
    G = nx.barabasi_albert_graph(100,1)

    pos = nx.spring_layout(G,iterations=500)
    draw_console(G,pos,padding=10,node_radius=0)

    nx.draw(G,pos)
    pl.show()
